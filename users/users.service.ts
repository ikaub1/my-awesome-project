import { UserEntity } from "types/User";
const User = require("./../models/user.model");

class UsersService {
  getUsers = async () => await User.find();

  getUserById = async (id: string) => await User.findById(id);

  deleteUserById = async (id: string) => await User.findByIdAndDelete(id);

  updateUserById = async (id: string, user: Partial<UserEntity>) =>
    await User.findByIdAndUpdate(id, { $set: { ...user } });
}

const usersService = new UsersService();

module.exports = usersService;
