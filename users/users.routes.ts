import { Router } from "express";
import passport from "passport";

const router = Router();

const usersController = require("./users.controller");
const usersMiddleware = require("./users.middleware");

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  usersController.getAllUsers
);

router.get(
  "/:id",
  [
    usersMiddleware.validateUserExists,
    passport.authenticate("jwt", { session: false }),
  ],
  usersController.getUserById
);

router.delete(
  "/:id",
  [
    usersMiddleware.validateUserExists,
    passport.authenticate("jwt", { session: false }),
  ],
  usersController.deleteUserById
);

router.patch(
  "/:id",
  [
    usersMiddleware.validateUserExists,
    passport.authenticate("jwt", { session: false }),
  ],
  usersController.updateUserById
);

module.exports = router;
