import { Request, Response, NextFunction } from "express";
import { UserEntity } from "./users.types";

const usersService = require("./users.service");

class UsersMiddleware {
  validateUserExists = (req: Request, res: Response, next: NextFunction) => {
    const { id } = req.params;
    if (!id || !usersService.getUserById(id)) {
      return res.status(400).json({ error: "Such user does not exist" });
    }
    next();
  };

  validateUserDoesNotExist = (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const { email } = req.body;
    const user = usersService
      .getUsers()
      .find((user: UserEntity) => user.email === email);
    if (user) {
      return res.status(400).json({ error: "Such user already exists" });
    }
    next();
  };
}

const usersMiddleware = new UsersMiddleware();

module.exports = usersMiddleware;
