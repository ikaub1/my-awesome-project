import { Request, Response } from "express";

const usersService = require("./users.service");

class UsersController {
  getAllUsers = async (req: Request, res: Response) => {
    try {
      res.send(await usersService.getUsers());
    } catch (e) {
      res.status(500).json({ error: "Some error occured, try again later" });
    }
  };

  getUserById = async (req: Request, res: Response) => {
    try {
      const { id } = req.params;
      res.send(await usersService.getUserById(id));
    } catch (e) {
      res.status(500).json({ error: "Some error occured, try again later" });
    }
  };

  deleteUserById = async (req: Request, res: Response) => {
    try {
      const { id } = req.params;
      await usersService.deleteUserById(id);
      res.status(204).send();
    } catch (e) {
      res.status(500).json({ error: "Some error occured, try again later" });
    }
  };

  updateUserById = async (req: Request, res: Response) => {
    try {
      const { id } = req.params;
      const { user } = req.body;
      res.send(await usersService.updateUserById(id, user));
    } catch (e) {
      res.status(500).json({ error: "Some error occured, try again later" });
    }
  };
}

const usersController = new UsersController();

module.exports = usersController;
