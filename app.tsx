import { NextFunction, Request, Response } from "express";
const express = require("express");
const fs = require("fs");
const path = require("path");
const mongoose = require("mongoose");
const cors = require("cors");
const passport = require("passport");
const compression = require("compression");
require("dotenv").config();

import React from "react";
import ReactDOMServer from "react-dom/server";
const { StaticRouter } = require("react-router-dom/server");
const { App } = require("./client/App");
import { applyPassportStrategy } from "./utils/passport";

const DIST_DIR = path.join(__dirname, "./");
const HTML_FILE = path.join(DIST_DIR, "client.html");

const usersRouter = require("./users/users.routes");
const authRouter = require("./auth/auth.routes");
import newsRouter from "./news/news.routes";
import emailsRouter from "./emails/emails.routes";

const PORT = 4000;

const app = express();

app.use(compression());

const apiRouter = express.Router();
apiRouter.use("/users", usersRouter);
apiRouter.use("/auth", authRouter);
apiRouter.use("/news", newsRouter);
apiRouter.use("/emails", emailsRouter);

app.use(cors());
app.use(passport.initialize());
app.use(express.json({ extended: true }));
app.use(express.urlencoded({ extended: true }));
app.use(express.static(DIST_DIR));

applyPassportStrategy();

app.use("/api", apiRouter);

app.use("/*", (req: Request, res: Response) => {
  fs.readFile(HTML_FILE, "utf-8", (err: Error, data: string) => {
    if (err) {
      console.error(err);
      return res.status(500).send("Some error happened");
    }

    return res.send(
      data.replace(
        '<div id="root"></div>',
        `<div id="root">
          ${ReactDOMServer.renderToString(
            <StaticRouter location={req.url}>
              <App />
            </StaticRouter>
          )}
        </div>`
      )
    );
  });
});

mongoose
  .connect(process.env.MONGO_CONNECTION_STRING, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    app.listen(PORT, () => {
      console.log(`Listening on port ${PORT}...`);
    });
  });
