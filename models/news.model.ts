import { Schema, model } from "mongoose";
import { NewsEntity } from "types/News";

const NewsSchema = new Schema<NewsEntity>({
  title: {
    unique: true,
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
});

export default model<NewsEntity>("News", NewsSchema);
