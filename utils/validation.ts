import { check, validationResult } from "express-validator";
import { Request } from "express";

const EMAIL_IS_EMPTY = "Email cannot be empty";
const EMAIL_IS_INVALID = "Email is invalid";

const PASSWORD_IS_EMPTY = "Password cannot be empty";
const PASSWORD_IS_INVALID = "Password length must be more than 8 characters";

const FIRST_NAME_IS_EMPTY = "First name cannot be empty";
const LAST_NAME_IS_EMPTY = "Last name cannot be empty";
const MIDDLE_NAME_IS_EMPTY = "Middle name cannot be empty";

export const registerValidation = [
  check("user.email")
    .exists()
    .withMessage(EMAIL_IS_EMPTY)
    .isEmail()
    .withMessage(EMAIL_IS_INVALID),
  check("user.password")
    .exists()
    .withMessage(PASSWORD_IS_EMPTY)
    .isLength({ min: 8 })
    .withMessage(PASSWORD_IS_INVALID),
  check("user.firstName").exists().withMessage(FIRST_NAME_IS_EMPTY),
  check("user.lastName").exists().withMessage(LAST_NAME_IS_EMPTY),
  check("user.middleName").exists().withMessage(MIDDLE_NAME_IS_EMPTY),
];

export const loginValidation = [
  check("email")
    .exists()
    .withMessage(EMAIL_IS_EMPTY)
    .isEmail()
    .withMessage(EMAIL_IS_INVALID),
  check("password")
    .exists()
    .withMessage(PASSWORD_IS_EMPTY)
    .isLength({ min: 8 })
    .withMessage(PASSWORD_IS_INVALID),
];

export const getValidationErrors = (req: Request) => {
  const validationErrors = validationResult(req);
  if (!validationErrors.isEmpty()) {
    return validationErrors.mapped();
  }
  return null;
};
