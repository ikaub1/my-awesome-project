const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "dimanovikov22@gmail.com",
    pass: "lbvjxrf220601",
  },
});

interface EmailConfig {
  email: string;
  destinationEmail: string;
  message: string;
}

const sendMail = async ({ email, destinationEmail, message }: EmailConfig) => {
  try {
    const mailOptions = {
      from: email,
      to: destinationEmail,
      subject: `${email} sent you new message`,
      html: `<p>${message}</p>`,
    };
    transporter.sendMail(mailOptions);
  } catch (e) {
    console.error(e);
  }
};

module.exports = { sendMail };
