"use strict";
exports.__esModule = true;
exports.applyPassportStrategy = void 0;
var passport_jwt_1 = require("passport-jwt");
var passport_1 = require("passport");
var User = require("./../models/user.model");
var applyPassportStrategy = function () {
    var options = {
        jwtFromRequest: passport_jwt_1.ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET
    };
    passport_1["default"].use(new passport_jwt_1.Strategy(options, function (payload, done) {
        User.findOne({ email: payload.email }, function (err, user) {
            if (err)
                return done(err, false);
            if (user) {
                return done(null, {
                    email: user.email
                });
            }
            return done(null, false);
        });
    }));
};
exports.applyPassportStrategy = applyPassportStrategy;
