import { Strategy, ExtractJwt } from "passport-jwt";
import passport from "passport";

import { UserEntity } from "users/users.types";
const User = require("./../models/user.model");

export const applyPassportStrategy = () => {
  const options = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.JWT_SECRET,
  };
  passport.use(
    new Strategy(options, (payload, done) => {
      User.findOne({ email: payload.email }, (err: Error, user: UserEntity) => {
        if (err) return done(err, false);
        if (user) {
          return done(null, {
            email: user.email,
          });
        }
        return done(null, false);
      });
    })
  );
};
