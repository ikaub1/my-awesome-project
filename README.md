### Novykov Dmytro, AM-4

Project is implemented using Typescript and Express.

Please, use Webpack to create a bundle or use following commands:

`npm install`

`npm start`

or

`webpack --mode=production && node dist/index.js` in the root directory of the project
