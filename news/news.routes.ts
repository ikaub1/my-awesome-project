import { Router } from "express";

import newsController from "./news.controller";

const router = Router();

router.get("/", newsController.getNews);

router.post("/", newsController.addNews);

router.delete("/:id", newsController.deleteNews);

export default router;
