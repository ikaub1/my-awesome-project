import { Request, Response } from "express";

import newsService from "./news.service";

class NewsController {
  getNews = async (req: Request, res: Response) => {
    const news = await newsService.getNews();
    res.json({ news });
  };

  addNews = async (req: Request, res: Response) => {
    const { title, description } = req.body;
    const news = await newsService.addNews({ title, description });
    res.status(201).json({ news });
  };

  deleteNews = async (req: Request, res: Response) => {
    const { id } = req.params;
    await newsService.deleteNews(id);
    res.status(204).send();
  };
}

const newsController = new NewsController();

export default newsController;
