import News from "./../models/news.model";
import { NewsEntity } from "types/News";

class NewsService {
  getNews = async () => await News.find();

  addNews = async (news: Partial<NewsEntity>) =>
    await new News({ ...news }).save();

  deleteNews = async (id: string) => await News.findByIdAndDelete(id);
}

const newsService = new NewsService();

export default newsService;
