const nodeExternals = require("webpack-node-externals");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");
const { SourceMapDevToolPlugin } = require("webpack");
const webpack = require("webpack");
const path = require("path");
const CompressionPlugin = require("compression-webpack-plugin");

const htmlPlugin = new HtmlWebPackPlugin({
  template: "./client/index.html",
  filename: "./client.html",
  chunks: ["client"],
});

const client = {
  entry: {
    client: "./client/index.tsx",
  },
  output: {
    filename: "[name].[contenthash].js",
    libraryTarget: "this",
  },
  target: "web",
  module: {
    rules: [
      {
        test: /\.(js|jsx|tsx|ts)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
          },
          {
            loader: "ts-loader",
            options: {
              configFile: path.resolve("./client/tsconfig.json"),
            },
          },
        ],
      },
      {
        test: /\.s?css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
    ],
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js"],
    alias: {
      components: path.resolve(__dirname, "client/components"),
      store: path.join(__dirname, "client/store"),
      pages: path.join(__dirname, "client/pages"),
      api: path.join(__dirname, "client/api"),
      types: path.join(__dirname, "client/types"),
      utils: path.join(__dirname, "client/utils"),
    },
  },
  plugins: [
    htmlPlugin,
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css",
    }),
    new SourceMapDevToolPlugin({
      filename: "[file].map",
    }),
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify("production"),
      },
    }),
    new webpack.optimize.AggressiveMergingPlugin(),
    new CompressionPlugin({
      filename: "[name].gz",
      algorithm: "gzip",
      test: /\.ts$|\.js$|\.css$|\.tsx$|\.scss$|\.html$/,
      threshold: 10240,
      minRatio: 0.8,
    }),
  ],
  mode: "production",
  optimization: {
    runtimeChunk: true,
    splitChunks: {
      cacheGroups: {
        vendor: {
          chunks: "initial",
          test: path.join(__dirname, "node_modules"),
          name: "vendor",
          enforce: true,
          chunks: "all",
        },
      },
    },
    minimizer: [
      new UglifyJSPlugin({
        uglifyOptions: {
          sourceMap: true,
          compress: {
            drop_console: true,
            conditionals: true,
            unused: true,
            comparisons: true,
            dead_code: true,
            if_return: true,
            join_vars: true,
          },
          output: {
            comments: false,
          },
        },
      }),
    ],
  },
};

const server = {
  entry: {
    server: "./app.tsx",
  },
  output: {
    filename: "[name].js",
    libraryTarget: "this",
  },
  target: "node",
  module: {
    rules: [
      {
        test: /\.(ts|tsx)?$/,
        loader: "ts-loader",
        options: {
          transpileOnly: true,
          configFile: "./tsconfig.json",
        },
      },
      {
        test: /\.s?css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
    ],
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js"],
    alias: {
      components: path.join(__dirname, "client/components"),
      store: path.join(__dirname, "client/store"),
      pages: path.join(__dirname, "client/pages"),
      api: path.join(__dirname, "client/api"),
      types: path.join(__dirname, "client/types"),
      utils: path.join(__dirname, "client/utils"),
    },
  },
  externals: [nodeExternals()],
  mode: "production",
  plugins: [
    htmlPlugin,
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css",
    }),
  ],
};

module.exports = [client, server];
