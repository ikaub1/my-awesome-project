import { Router } from "express";
import emailsController from "./emails.controller";

const router = Router();

router.post("/send", emailsController.sendEmail);

export default router;
