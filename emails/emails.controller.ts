import { Request, Response } from "express";

const { sendMail } = require("./../utils/emails-sender");

class EmailsController {
  sendEmail = async (req: Request, res: Response) => {
    const { from, to, message } = req.body;
    await sendMail({
      email: "dimanovikov22@gmail.com",
      destinationEmail: to,
      message,
    });
    res.status(204).send();
  };
}

const emailsController = new EmailsController();

export default emailsController;
