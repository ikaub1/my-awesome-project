import { Document } from "mongoose";

export interface NewsEntity extends Document {
  title: string;
  description: string;
}
