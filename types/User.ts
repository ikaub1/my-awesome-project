import { Document } from "mongoose";

export interface UserEntity extends Document {
  id: string;
  email: string;
  firstName: string;
  middleName: string;
  lastName: string;
  password: string;
}
