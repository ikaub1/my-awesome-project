"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const passport_1 = __importDefault(require("passport"));
const router = (0, express_1.Router)();
const usersController = require("./users.controller");
const usersMiddleware = require("./users.middleware");
router.get("/", passport_1.default.authenticate("jwt", { session: false }), usersController.getAllUsers);
router.get("/:id", [
    usersMiddleware.validateUserExists,
    passport_1.default.authenticate("jwt", { session: false }),
], usersController.getUserById);
router.delete("/:id", [
    usersMiddleware.validateUserExists,
    passport_1.default.authenticate("jwt", { session: false }),
], usersController.deleteUserById);
router.patch("/:id", [
    usersMiddleware.validateUserExists,
    passport_1.default.authenticate("jwt", { session: false }),
], usersController.updateUserById);
module.exports = router;
//# sourceMappingURL=users.routes.js.map