"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const usersService = require("./users.service");
class UsersMiddleware {
    constructor() {
        this.validateUserExists = (req, res, next) => {
            const { id } = req.params;
            if (!id || !usersService.getUserById(id)) {
                return res.status(400).json({ error: "Such user does not exist" });
            }
            next();
        };
        this.validateUserDoesNotExist = (req, res, next) => {
            const { email } = req.body;
            const user = usersService
                .getUsers()
                .find((user) => user.email === email);
            if (user) {
                return res.status(400).json({ error: "Such user already exists" });
            }
            next();
        };
    }
}
const usersMiddleware = new UsersMiddleware();
module.exports = usersMiddleware;
//# sourceMappingURL=users.middleware.js.map