"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const User = require("./../models/user.model");
class UsersService {
    constructor() {
        this.getUsers = () => __awaiter(this, void 0, void 0, function* () { return yield User.find(); });
        this.getUserById = (id) => __awaiter(this, void 0, void 0, function* () { return yield User.findById(id); });
        this.deleteUserById = (id) => __awaiter(this, void 0, void 0, function* () { return yield User.findByIdAndDelete(id); });
        this.updateUserById = (id, user) => __awaiter(this, void 0, void 0, function* () { return yield User.findByIdAndUpdate(id, { $set: Object.assign({}, user) }); });
    }
}
const usersService = new UsersService();
module.exports = usersService;
//# sourceMappingURL=users.service.js.map