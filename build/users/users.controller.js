"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const usersService = require("./users.service");
class UsersController {
    constructor() {
        this.getAllUsers = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                res.send(yield usersService.getUsers());
            }
            catch (e) {
                res.status(500).json({ error: "Some error occured, try again later" });
            }
        });
        this.getUserById = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                res.send(yield usersService.getUserById(id));
            }
            catch (e) {
                res.status(500).json({ error: "Some error occured, try again later" });
            }
        });
        this.deleteUserById = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                yield usersService.deleteUserById(id);
                res.status(204).send();
            }
            catch (e) {
                res.status(500).json({ error: "Some error occured, try again later" });
            }
        });
        this.updateUserById = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const { user } = req.body;
                res.send(yield usersService.updateUserById(id, user));
            }
            catch (e) {
                res.status(500).json({ error: "Some error occured, try again later" });
            }
        });
    }
}
const usersController = new UsersController();
module.exports = usersController;
//# sourceMappingURL=users.controller.js.map