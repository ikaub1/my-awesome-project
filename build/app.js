"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const fs = require("fs");
const path = require("path");
const mongoose = require("mongoose");
const cors = require("cors");
const passport = require("passport");
require("dotenv").config();
const react_1 = __importDefault(require("react"));
const server_1 = __importDefault(require("react-dom/server"));
const { StaticRouter } = require("react-router-dom/server");
const { App } = require("./client/App");
const passport_1 = require("./utils/passport");
const DIST_DIR = path.join(__dirname, "./");
const HTML_FILE = path.join(DIST_DIR, "client.html");
const usersRouter = require("./users/users.routes");
const authRouter = require("./auth/auth.routes");
const PORT = 4000;
const app = express();
const apiRouter = express.Router();
apiRouter.use("/users", usersRouter);
apiRouter.use("/auth", authRouter);
app.use(cors());
app.use(passport.initialize());
app.use(express.json({ extended: true }));
app.use(express.urlencoded({ extended: true }));
app.use(express.static(DIST_DIR));
(0, passport_1.applyPassportStrategy)();
app.use("/api", apiRouter);
app.use("/*", (req, res) => {
    fs.readFile(HTML_FILE, "utf-8", (err, data) => {
        if (err) {
            console.error(err);
            return res.status(500).send("Some error happened");
        }
        return res.send(data.replace('<div id="root"></div>', `<div id="root">
          ${server_1.default.renderToString(react_1.default.createElement(StaticRouter, { location: req.url },
            react_1.default.createElement(App, null)))}
        </div>`));
    });
});
mongoose
    .connect(process.env.MONGO_CONNECTION_STRING, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})
    .then(() => {
    app.listen(PORT, () => {
        console.log(`Listening on port ${PORT}...`);
    });
});
//# sourceMappingURL=app.js.map