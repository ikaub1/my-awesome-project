"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const validation_1 = require("./../utils/validation");
const authController = require("./auth.controller");
const router = (0, express_1.Router)();
router.post("/sign-up", validation_1.registerValidation, authController.register);
router.post("/sign-in", validation_1.loginValidation, authController.login);
module.exports = router;
//# sourceMappingURL=auth.routes.js.map