"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const User = require("./../models/user.model");
class AuthService {
    constructor() {
        this.login = (email, password) => __awaiter(this, void 0, void 0, function* () {
            const user = yield User.findOne({ email });
            if (user === null || user === void 0 ? void 0 : user.email) {
                const isPasswordValid = yield user.isPasswordValid(password);
                if (isPasswordValid) {
                    const token = jsonwebtoken_1.default.sign({ email }, process.env.JWT_SECRET || "", {
                        expiresIn: 1000000,
                    });
                    const userToReturn = Object.assign(Object.assign({}, user.toJSON()), { token });
                    delete userToReturn.password;
                    return userToReturn;
                }
                else {
                    throw "Credentials are invalid";
                }
            }
            else {
                throw "Credentials are invalid";
            }
        });
        this.register = (user) => __awaiter(this, void 0, void 0, function* () {
            const candidate = yield User.findOne({ email: user.email });
            if (!candidate) {
                yield User(Object.assign(Object.assign({}, user), { roles: ["user"] })).save();
                const createdUser = yield User.findOne({ email: user.email });
                const token = jsonwebtoken_1.default.sign({ email: createdUser.email }, process.env.JWT_SECRET || "", {
                    expiresIn: 1000000,
                });
                const userToReturn = Object.assign(Object.assign({}, createdUser.toJSON()), { token });
                delete userToReturn.password;
                return userToReturn;
            }
            else {
                throw "User already exists";
            }
        });
    }
}
const authService = new AuthService();
module.exports = authService;
//# sourceMappingURL=auth.service.js.map