"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const validation_1 = require("./../utils/validation");
const authService = require("./auth.service");
class AuthController {
    constructor() {
        this.login = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const errors = (0, validation_1.getValidationErrors)(req);
                if (errors) {
                    return res.status(400).json({
                        errors,
                    });
                }
                const { email, password } = req.body;
                const user = yield authService.login(email, password);
                res.json({ user });
            }
            catch (error) {
                res.status(400).json({ error });
            }
        });
        this.register = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const errors = (0, validation_1.getValidationErrors)(req);
                if (errors) {
                    return res.status(400).json({
                        errors,
                    });
                }
                const { user } = req.body;
                const createdUser = yield authService.register(user);
                res.status(201).json({ user: createdUser });
            }
            catch (error) {
                res.status(400).json({ error });
            }
        });
    }
}
const authController = new AuthController();
module.exports = authController;
//# sourceMappingURL=auth.controller.js.map