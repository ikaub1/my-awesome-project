declare class RequestCreator {
    get: (url: string) => Promise<any>;
    post: (url: string, data: unknown) => Promise<any>;
    delete: (url: string) => Promise<any>;
    patch: (url: string, data: unknown) => Promise<any>;
}
declare const requestCreator: RequestCreator;
export default requestCreator;
//# sourceMappingURL=RequestCreator.d.ts.map