"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class API {
    constructor() {
        this.login = () => `/auth/sign-in`;
        this.users = () => `/users`;
    }
}
const api = new API();
exports.default = api;
//# sourceMappingURL=API.js.map