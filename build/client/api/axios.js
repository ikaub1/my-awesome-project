"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const instance = axios_1.default.create({
    baseURL: "http://localhost:4000/api",
    headers: {
        "Content-Type": "application/json",
    },
});
const requestHandler = (request) => {
    request.headers = Object.assign(Object.assign({}, request.headers), { Authorization: `Bearer ${localStorage.getItem("token")}` || "" });
    return request;
};
const responseHandler = (response) => {
    if (response.status === 401) {
        window.location.href = "/";
    }
    return response;
};
const errorHandler = (error) => {
    return Promise.reject(error);
};
instance.interceptors.request.use(requestHandler, errorHandler);
instance.interceptors.response.use(responseHandler, errorHandler);
exports.default = instance;
//# sourceMappingURL=axios.js.map