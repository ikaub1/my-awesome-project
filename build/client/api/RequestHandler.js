"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createRequestHandler = void 0;
const effects_1 = require("@redux-saga/core/effects");
const createRequestHandler = (callFn, action) => {
    return function* (url, data) {
        try {
            const result = yield callFn(url, data);
            yield (0, effects_1.put)(action.done({ result }));
        }
        catch (error) {
            yield (0, effects_1.put)(action.failed({ params: {}, error: {} }));
        }
    };
};
exports.createRequestHandler = createRequestHandler;
//# sourceMappingURL=RequestHandler.js.map