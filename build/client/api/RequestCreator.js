"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("./axios"));
class RequestCreator {
    constructor() {
        this.get = (url) => __awaiter(this, void 0, void 0, function* () {
            const response = yield axios_1.default.get(url);
            return response.data;
        });
        this.post = (url, data) => __awaiter(this, void 0, void 0, function* () {
            const response = yield axios_1.default.post(url, data);
            return response.data;
        });
        this.delete = (url) => __awaiter(this, void 0, void 0, function* () {
            const response = yield axios_1.default.delete(url);
            return response.data;
        });
        this.patch = (url, data) => __awaiter(this, void 0, void 0, function* () {
            const response = yield axios_1.default.patch(url, data);
            return response.data;
        });
    }
}
const requestCreator = new RequestCreator();
exports.default = requestCreator;
//# sourceMappingURL=RequestCreator.js.map