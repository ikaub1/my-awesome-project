import { AsyncActionCreators } from "typescript-fsa";
import { SagaIterator } from "@redux-saga/types";
export declare const createRequestHandler: (callFn: CallableFunction, action: AsyncActionCreators<any, any>) => (url: string, data?: unknown) => SagaIterator<any>;
//# sourceMappingURL=RequestHandler.d.ts.map