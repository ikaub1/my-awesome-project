"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Modal = void 0;
const react_1 = __importDefault(require("react"));
const react_redux_1 = require("react-redux");
const Modal = ({ children }) => {
    const dispatch = (0, react_redux_1.useDispatch)();
    // if (!opened) return null;
    // const handleClose = () => {
    //   dispatch(toggleModal({ modal: "" }));
    // };
    return (react_1.default.createElement("div", { className: "modal" },
        react_1.default.createElement("div", { className: "modal__content" }, children)));
};
exports.Modal = Modal;
//# sourceMappingURL=index.js.map