import React from "react";
interface ButtonProps extends React.HTMLProps<HTMLButtonElement> {
    type?: "button" | "submit" | "reset";
    className?: string;
    theme?: "default" | "delete";
}
export declare const Button: React.FC<ButtonProps>;
export {};
//# sourceMappingURL=Button.d.ts.map