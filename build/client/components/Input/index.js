"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Input = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const Input = (_a) => {
    var { label, value, className = "", error = "" } = _a, rest = __rest(_a, ["label", "value", "className", "error"]);
    return (react_1.default.createElement("label", { className: (0, classnames_1.default)("input", className) },
        react_1.default.createElement("input", Object.assign({ className: "input__control" }, rest)),
        react_1.default.createElement("span", { className: (0, classnames_1.default)("input__label", { "input__label-value": !!value }) }, label),
        error && react_1.default.createElement("span", { className: "input__error" }, error)));
};
exports.Input = Input;
//# sourceMappingURL=index.js.map