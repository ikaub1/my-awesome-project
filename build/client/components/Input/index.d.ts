import React from "react";
interface InputProps extends React.HTMLProps<HTMLInputElement> {
    label?: string;
    className?: string;
    error?: string;
}
export declare const Input: React.FC<InputProps>;
export {};
//# sourceMappingURL=index.d.ts.map