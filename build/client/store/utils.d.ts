export declare const createAction: import("typescript-fsa").ActionCreatorFactory;
export declare const createActionAsync: <Params, Result, Error_1 = {}>(type: string, commonMeta?: import("typescript-fsa").Meta | undefined) => import("typescript-fsa").AsyncActionCreators<Params, Result, Error_1>;
//# sourceMappingURL=utils.d.ts.map