declare const rootReducer: import("redux").Reducer<import("redux").CombinedState<{
    auth: import("./auth/auth.reducer").AuthState;
    users: import("./users/users.reducer").UsersState;
}>, import("redux").AnyAction>;
export declare type RootState = ReturnType<typeof rootReducer>;
export declare const store: import("redux").Store<import("redux").EmptyObject & {
    auth: import("./auth/auth.reducer").AuthState;
    users: import("./users/users.reducer").UsersState;
}, import("redux").AnyAction> & {
    dispatch: unknown;
};
export {};
//# sourceMappingURL=index.d.ts.map