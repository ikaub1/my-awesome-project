"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.store = void 0;
const effects_1 = require("@redux-saga/core/effects");
const redux_1 = require("redux");
const redux_saga_1 = __importDefault(require("redux-saga"));
const auth_reducer_1 = require("./auth/auth.reducer");
const auth_watchers_1 = require("./auth/auth.watchers");
const users_reducer_1 = require("./users/users.reducer");
const users_watchers_1 = require("./users/users.watchers");
function* rootSaga() {
    yield (0, effects_1.all)([(0, effects_1.fork)(auth_watchers_1.authWatchers), (0, effects_1.fork)(users_watchers_1.usersWatchers)]);
}
const rootReducer = (0, redux_1.combineReducers)({
    auth: auth_reducer_1.authReducer,
    users: users_reducer_1.usersReducer,
});
const sagaMiddleware = (0, redux_saga_1.default)();
exports.store = (0, redux_1.createStore)(rootReducer, (0, redux_1.applyMiddleware)(sagaMiddleware));
sagaMiddleware.run(rootSaga);
//# sourceMappingURL=index.js.map