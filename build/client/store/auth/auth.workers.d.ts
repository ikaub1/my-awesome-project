import { SagaIterator } from "@redux-saga/types";
import { signIn } from "./auth.actions";
export declare function signInWorker(action: ReturnType<typeof signIn.started>): SagaIterator;
//# sourceMappingURL=auth.workers.d.ts.map