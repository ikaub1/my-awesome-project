"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.signInWorker = void 0;
const effects_1 = require("@redux-saga/core/effects");
const api_1 = require("./../../api");
const auth_actions_1 = require("./auth.actions");
const loginRequest = (0, api_1.createRequestHandler)(api_1.requestCreator.post, auth_actions_1.signIn);
function* signInWorker(action) {
    const { email, password } = action.payload;
    yield (0, effects_1.call)(loginRequest, api_1.API.login(), { email, password });
}
exports.signInWorker = signInWorker;
//# sourceMappingURL=auth.workers.js.map