import { User } from "./../../types/User";
export declare type AuthState = {
    user: User | null;
};
export declare const authReducer: import("typescript-fsa-reducers").ReducerBuilder<AuthState, AuthState, AuthState | undefined>;
//# sourceMappingURL=auth.reducer.d.ts.map