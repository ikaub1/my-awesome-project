"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authReducer = void 0;
const typescript_fsa_reducers_1 = require("typescript-fsa-reducers");
const auth_actions_1 = require("./auth.actions");
const initialState = {
    user: null,
};
exports.authReducer = (0, typescript_fsa_reducers_1.reducerWithInitialState)(initialState).case(auth_actions_1.signIn.done, (state, { result }) => {
    localStorage.setItem("token", result.user.token);
    return Object.assign(Object.assign({}, state), result);
});
//# sourceMappingURL=auth.reducer.js.map