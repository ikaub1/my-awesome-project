"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authWatchers = exports.signInWatcher = void 0;
const effects_1 = require("@redux-saga/core/effects");
const auth_actions_1 = require("./auth.actions");
const auth_workers_1 = require("./auth.workers");
function* signInWatcher() {
    yield (0, effects_1.takeLatest)(auth_actions_1.signIn.started, auth_workers_1.signInWorker);
}
exports.signInWatcher = signInWatcher;
function* authWatchers() {
    yield (0, effects_1.all)([(0, effects_1.fork)(signInWatcher)]);
}
exports.authWatchers = authWatchers;
//# sourceMappingURL=auth.watchers.js.map