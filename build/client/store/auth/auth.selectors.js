"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.selectUser = void 0;
const reselect_1 = require("reselect");
const authSelector = (state) => state.auth;
exports.selectUser = (0, reselect_1.createSelector)(authSelector, (state) => state.user);
//# sourceMappingURL=auth.selectors.js.map