import { User } from "./../../types/User";
declare type SignInStarted = {
    email: string;
    password: string;
};
declare type SignInDone = {
    user: User;
};
export declare const signIn: import("typescript-fsa").AsyncActionCreators<SignInStarted, SignInDone, {}>;
export {};
//# sourceMappingURL=auth.actions.d.ts.map