"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createActionAsync = exports.createAction = void 0;
const typescript_fsa_1 = __importDefault(require("typescript-fsa"));
exports.createAction = (0, typescript_fsa_1.default)();
exports.createActionAsync = exports.createAction.async;
//# sourceMappingURL=utils.js.map