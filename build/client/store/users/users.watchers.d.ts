import { SagaIterator } from "@redux-saga/types";
export declare function fetchUsersWatcher(): SagaIterator;
export declare function deleteUserWatcher(): SagaIterator;
export declare function usersWatchers(): SagaIterator;
//# sourceMappingURL=users.watchers.d.ts.map