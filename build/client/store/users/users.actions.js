"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteUser = exports.fetchUsers = void 0;
const utils_1 = require("./../../store/utils");
exports.fetchUsers = (0, utils_1.createActionAsync)("users => fetchUsers");
exports.deleteUser = (0, utils_1.createActionAsync)("users => DELETE_USER");
//# sourceMappingURL=users.actions.js.map