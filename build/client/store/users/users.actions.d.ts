import { User } from "./../../types/User";
declare type FetchUsersDone = User[];
export declare const fetchUsers: import("typescript-fsa").AsyncActionCreators<void, FetchUsersDone, {}>;
declare type DeleteUserStarted = {
    userId: string;
};
export declare const deleteUser: import("typescript-fsa").AsyncActionCreators<DeleteUserStarted, void, {}>;
export {};
//# sourceMappingURL=users.actions.d.ts.map