"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.usersWatchers = exports.deleteUserWatcher = exports.fetchUsersWatcher = void 0;
const effects_1 = require("@redux-saga/core/effects");
const users_actions_1 = require("./users.actions");
const users_workers_1 = require("./users.workers");
function* fetchUsersWatcher() {
    yield (0, effects_1.takeLatest)(users_actions_1.fetchUsers.started, users_workers_1.fetchUsersWorker);
}
exports.fetchUsersWatcher = fetchUsersWatcher;
function* deleteUserWatcher() {
    yield (0, effects_1.takeLatest)(users_actions_1.deleteUser.started, users_workers_1.deleteUserWorker);
}
exports.deleteUserWatcher = deleteUserWatcher;
function* usersWatchers() {
    yield (0, effects_1.all)([(0, effects_1.fork)(fetchUsersWatcher), (0, effects_1.fork)(deleteUserWatcher)]);
}
exports.usersWatchers = usersWatchers;
//# sourceMappingURL=users.watchers.js.map