import { SagaIterator } from "@redux-saga/types";
export declare function fetchUsersWorker(): SagaIterator;
export declare function deleteUserWorker(): SagaIterator;
//# sourceMappingURL=users.workers.d.ts.map