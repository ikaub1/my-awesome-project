"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteUserWorker = exports.fetchUsersWorker = void 0;
const effects_1 = require("@redux-saga/core/effects");
const api_1 = require("./../../api");
const users_actions_1 = require("./users.actions");
const fetchUsersRequest = (0, api_1.createRequestHandler)(api_1.requestCreator.get, users_actions_1.fetchUsers);
function* fetchUsersWorker() {
    yield (0, effects_1.call)(fetchUsersRequest, api_1.API.users());
}
exports.fetchUsersWorker = fetchUsersWorker;
function* deleteUserWorker() { }
exports.deleteUserWorker = deleteUserWorker;
//# sourceMappingURL=users.workers.js.map