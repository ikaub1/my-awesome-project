"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.selectUsers = void 0;
const reselect_1 = require("reselect");
const usersSelector = (state) => state.users;
exports.selectUsers = (0, reselect_1.createSelector)(usersSelector, (state) => state.users);
//# sourceMappingURL=users.selectors.js.map