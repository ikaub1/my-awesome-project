import { User } from "./../../types/User";
export declare type UsersState = {
    users: User[];
    isUserOperationStarted: boolean;
    userToOperate: string;
};
export declare const usersReducer: import("typescript-fsa-reducers").ReducerBuilder<UsersState, UsersState, UsersState | undefined>;
//# sourceMappingURL=users.reducer.d.ts.map