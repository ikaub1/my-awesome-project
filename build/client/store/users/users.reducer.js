"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.usersReducer = void 0;
const typescript_fsa_reducers_1 = require("typescript-fsa-reducers");
const users_actions_1 = require("./users.actions");
const initialState = {
    users: [],
    isUserOperationStarted: false,
    userToOperate: "",
};
exports.usersReducer = (0, typescript_fsa_reducers_1.reducerWithInitialState)(initialState)
    .case(users_actions_1.fetchUsers.done, (state, { result }) => (Object.assign(Object.assign({}, state), { users: result })))
    .case(users_actions_1.deleteUser.started, (state, { userId }) => (Object.assign(Object.assign({}, state), { isUserOperationStarted: true, userToOperate: userId })));
//# sourceMappingURL=users.reducer.js.map