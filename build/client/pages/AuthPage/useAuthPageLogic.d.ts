/// <reference types="react" />
declare type FormState = {
    email: string;
    password: string;
};
export declare const useAuthPageLogic: () => {
    initialValues: FormState;
    initialErrors: import("formik").FormikErrors<unknown>;
    initialTouched: import("formik").FormikTouched<unknown>;
    initialStatus: any;
    handleBlur: {
        (e: import("react").FocusEvent<any, Element>): void;
        <T = any>(fieldOrEvent: T): T extends string ? (e: any) => void : void;
    };
    handleChange: {
        (e: import("react").ChangeEvent<any>): void;
        <T_1 = string | import("react").ChangeEvent<any>>(field: T_1): T_1 extends import("react").ChangeEvent<any> ? void : (e: string | import("react").ChangeEvent<any>) => void;
    };
    handleReset: (e: any) => void;
    handleSubmit: (e?: import("react").FormEvent<HTMLFormElement> | undefined) => void;
    resetForm: (nextState?: Partial<import("formik").FormikState<FormState>> | undefined) => void;
    setErrors: (errors: import("formik").FormikErrors<FormState>) => void;
    setFormikState: (stateOrCb: import("formik").FormikState<FormState> | ((state: import("formik").FormikState<FormState>) => import("formik").FormikState<FormState>)) => void;
    setFieldTouched: (field: string, touched?: boolean | undefined, shouldValidate?: boolean | undefined) => Promise<void> | Promise<import("formik").FormikErrors<FormState>>;
    setFieldValue: (field: string, value: any, shouldValidate?: boolean | undefined) => Promise<void> | Promise<import("formik").FormikErrors<FormState>>;
    setFieldError: (field: string, value: string | undefined) => void;
    setStatus: (status: any) => void;
    setSubmitting: (isSubmitting: boolean) => void;
    setTouched: (touched: import("formik").FormikTouched<FormState>, shouldValidate?: boolean | undefined) => Promise<void> | Promise<import("formik").FormikErrors<FormState>>;
    setValues: (values: import("react").SetStateAction<FormState>, shouldValidate?: boolean | undefined) => Promise<void> | Promise<import("formik").FormikErrors<FormState>>;
    submitForm: () => Promise<any>;
    validateForm: (values?: FormState | undefined) => Promise<import("formik").FormikErrors<FormState>>;
    validateField: (name: string) => Promise<void> | Promise<string | undefined>;
    isValid: boolean;
    dirty: boolean;
    unregisterField: (name: string) => void;
    registerField: (name: string, { validate }: any) => void;
    getFieldProps: (nameOrOptions: any) => import("formik").FieldInputProps<any>;
    getFieldMeta: (name: string) => import("formik").FieldMetaProps<any>;
    getFieldHelpers: (name: string) => import("formik").FieldHelperProps<any>;
    validateOnBlur: boolean;
    validateOnChange: boolean;
    validateOnMount: boolean;
    values: FormState;
    errors: import("formik").FormikErrors<FormState>;
    touched: import("formik").FormikTouched<FormState>;
    isSubmitting: boolean;
    isValidating: boolean;
    status?: any;
    submitCount: number;
    user: import("../../types/User").User | null;
};
export {};
//# sourceMappingURL=useAuthPageLogic.d.ts.map