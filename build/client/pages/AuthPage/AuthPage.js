"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthPage = void 0;
const react_1 = __importDefault(require("react"));
const react_router_1 = require("react-router");
const useAuthPageLogic_1 = require("./useAuthPageLogic");
const Input_1 = require("components/Input");
const Button_1 = require("components/Button/Button");
const AuthPage = () => {
    const { user, handleSubmit, values, handleChange, errors } = (0, useAuthPageLogic_1.useAuthPageLogic)();
    if (user)
        return react_1.default.createElement(react_router_1.Navigate, { to: "/users" });
    return (react_1.default.createElement("div", { className: "auth" },
        react_1.default.createElement("h1", { className: "auth__title" }, "Welcome to the Email management system!"),
        react_1.default.createElement("h2", { className: "auth__subtitle" }, "Please, log in using the form below to start emailing"),
        react_1.default.createElement("form", { onSubmit: handleSubmit, className: "auth__form" },
            react_1.default.createElement(Input_1.Input, { value: values.email, onChange: handleChange, className: "auth__input", label: "Email", type: "email", name: "email", error: errors.email }),
            react_1.default.createElement(Input_1.Input, { value: values.password, onChange: handleChange, className: "auth__input", label: "Password", type: "password", name: "password", error: errors.password }),
            react_1.default.createElement(Button_1.Button, { type: "submit", className: "auth__btn" }, "Sign in"))));
};
exports.AuthPage = AuthPage;
//# sourceMappingURL=AuthPage.js.map