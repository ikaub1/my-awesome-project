"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.useAuthPageLogic = void 0;
const react_redux_1 = require("react-redux");
const formik_1 = require("formik");
const Yup = __importStar(require("yup"));
const auth_actions_1 = require("./../../store/auth/auth.actions");
const auth_selectors_1 = require("./../../store/auth/auth.selectors");
const useAuthPageLogic = () => {
    const dispatch = (0, react_redux_1.useDispatch)();
    const user = (0, react_redux_1.useSelector)(auth_selectors_1.selectUser);
    const handleSubmit = (values) => {
        dispatch(auth_actions_1.signIn.started(values));
    };
    const validationSchema = Yup.object({
        email: Yup.string()
            .required("Email cannot be empty")
            .email("Email is incorrect, try again please"),
        password: Yup.string()
            .required("Password cannot be empty")
            .min(8, "Password must be 8 characters in length minimum"),
    });
    const formik = (0, formik_1.useFormik)({
        initialValues: {
            email: "",
            password: "",
        },
        onSubmit: handleSubmit,
        validationSchema,
        validateOnChange: false,
        validateOnBlur: false,
    });
    return Object.assign({ user }, formik);
};
exports.useAuthPageLogic = useAuthPageLogic;
//# sourceMappingURL=useAuthPageLogic.js.map