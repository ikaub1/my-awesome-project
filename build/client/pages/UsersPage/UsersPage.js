"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersPage = void 0;
const react_1 = __importStar(require("react"));
const react_redux_1 = require("react-redux");
const users_selectors_1 = require("./../../store/users/users.selectors");
const users_actions_1 = require("./../../store/users/users.actions");
const UserCard_1 = require("./components/UserCard");
const UsersPage = () => {
    const dispatch = (0, react_redux_1.useDispatch)();
    const users = (0, react_redux_1.useSelector)(users_selectors_1.selectUsers);
    (0, react_1.useEffect)(() => {
        dispatch(users_actions_1.fetchUsers.started());
    }, []);
    const handleDeleteUser = (userId) => () => { };
    return (react_1.default.createElement("div", { className: "users" }, users.map((user) => (react_1.default.createElement(UserCard_1.UserCard, { onDelete: handleDeleteUser, key: user._id, user: user })))));
};
exports.UsersPage = UsersPage;
//# sourceMappingURL=UsersPage.js.map