import React from "react";
import { User } from "../../../../types/User";
interface UserCardProps {
    user: User;
    onDelete: (id: string) => () => void;
}
export declare const UserCard: React.FC<UserCardProps>;
export {};
//# sourceMappingURL=index.d.ts.map