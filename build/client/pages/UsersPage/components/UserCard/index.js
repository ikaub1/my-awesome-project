"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserCard = void 0;
const react_1 = __importDefault(require("react"));
const Button_1 = require("./../../../../components/Button/Button");
const displayConfig = {
    email: "Email",
    firstName: "First Name",
    middleName: "Middle Name",
    lastName: "Last Name",
};
const getUserRoles = (roles) => {
    return roles.length > 1 ? roles.join(", ") : roles[0];
};
const UserCard = ({ user, onDelete, }) => {
    return (react_1.default.createElement("div", { className: "user-card" },
        react_1.default.createElement("h2", { className: "user-card__roles" },
            "Roles: ",
            getUserRoles(user.roles)),
        react_1.default.createElement("table", { className: "user-card__table" },
            react_1.default.createElement("tbody", null, Object.entries(displayConfig).map(([value, label]) => (react_1.default.createElement("tr", { className: "user-card__table-row", key: value },
                react_1.default.createElement("th", { className: "user-card__table-title" }, label),
                react_1.default.createElement("td", { className: "user-card__table-data" }, user[value])))))),
        react_1.default.createElement("div", { className: "user-card__btn-container" },
            react_1.default.createElement(Button_1.Button, { onClick: onDelete(user._id), theme: "delete" }, "Delete user"),
            react_1.default.createElement(Button_1.Button, { className: "user-card__send" }, "Send email"))));
};
exports.UserCard = UserCard;
//# sourceMappingURL=index.js.map