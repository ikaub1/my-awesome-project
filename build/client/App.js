"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.App = void 0;
const react_1 = __importDefault(require("react"));
const react_router_dom_1 = require("react-router-dom");
const react_redux_1 = require("react-redux");
require("./App.scss");
const pages_1 = require("./pages");
const store_1 = require("./store");
const auth_selectors_1 = require("./store/auth/auth.selectors");
const Modal_1 = require("./components/Modal");
const ProtectedRoute = ({ element }) => {
    const user = (0, react_redux_1.useSelector)(auth_selectors_1.selectUser);
    return user && element ? element : react_1.default.createElement(react_router_dom_1.Navigate, { to: "/" });
};
const App = () => {
    return (react_1.default.createElement(react_redux_1.Provider, { store: store_1.store },
        react_1.default.createElement(Modal_1.Modal, null),
        react_1.default.createElement(react_router_dom_1.Routes, null,
            react_1.default.createElement(react_router_dom_1.Route, { path: "/", element: react_1.default.createElement(pages_1.AuthPage, null) }),
            react_1.default.createElement(react_router_dom_1.Route, { path: "/users", element: react_1.default.createElement(ProtectedRoute, { element: react_1.default.createElement(pages_1.UsersPage, null) }) }),
            react_1.default.createElement(react_router_dom_1.Route, { path: "*", element: react_1.default.createElement(react_router_dom_1.Navigate, { to: "/" }) }))));
};
exports.App = App;
//# sourceMappingURL=App.js.map