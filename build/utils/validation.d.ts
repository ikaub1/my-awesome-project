import { Request } from "express";
export declare const registerValidation: import("express-validator").ValidationChain[];
export declare const loginValidation: import("express-validator").ValidationChain[];
export declare const getValidationErrors: (req: Request) => Record<string, import("express-validator").ValidationError> | null;
//# sourceMappingURL=validation.d.ts.map