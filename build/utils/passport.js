"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.applyPassportStrategy = void 0;
const passport_jwt_1 = require("passport-jwt");
const passport_1 = __importDefault(require("passport"));
const User = require("./../models/user.model");
const applyPassportStrategy = () => {
    const options = {
        jwtFromRequest: passport_jwt_1.ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET,
    };
    passport_1.default.use(new passport_jwt_1.Strategy(options, (payload, done) => {
        User.findOne({ email: payload.email }, (err, user) => {
            if (err)
                return done(err, false);
            if (user) {
                return done(null, {
                    email: user.email,
                });
            }
            return done(null, false);
        });
    }));
};
exports.applyPassportStrategy = applyPassportStrategy;
//# sourceMappingURL=passport.js.map