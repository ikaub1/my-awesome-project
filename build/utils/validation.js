"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getValidationErrors = exports.loginValidation = exports.registerValidation = void 0;
const express_validator_1 = require("express-validator");
const EMAIL_IS_EMPTY = "Email cannot be empty";
const EMAIL_IS_INVALID = "Email is invalid";
const PASSWORD_IS_EMPTY = "Password cannot be empty";
const PASSWORD_IS_INVALID = "Password length must be more than 8 characters";
const FIRST_NAME_IS_EMPTY = "First name cannot be empty";
const LAST_NAME_IS_EMPTY = "Last name cannot be empty";
const MIDDLE_NAME_IS_EMPTY = "Middle name cannot be empty";
exports.registerValidation = [
    (0, express_validator_1.check)("user.email")
        .exists()
        .withMessage(EMAIL_IS_EMPTY)
        .isEmail()
        .withMessage(EMAIL_IS_INVALID),
    (0, express_validator_1.check)("user.password")
        .exists()
        .withMessage(PASSWORD_IS_EMPTY)
        .isLength({ min: 8 })
        .withMessage(PASSWORD_IS_INVALID),
    (0, express_validator_1.check)("user.firstName").exists().withMessage(FIRST_NAME_IS_EMPTY),
    (0, express_validator_1.check)("user.lastName").exists().withMessage(LAST_NAME_IS_EMPTY),
    (0, express_validator_1.check)("user.middleName").exists().withMessage(MIDDLE_NAME_IS_EMPTY),
];
exports.loginValidation = [
    (0, express_validator_1.check)("email")
        .exists()
        .withMessage(EMAIL_IS_EMPTY)
        .isEmail()
        .withMessage(EMAIL_IS_INVALID),
    (0, express_validator_1.check)("password")
        .exists()
        .withMessage(PASSWORD_IS_EMPTY)
        .isLength({ min: 8 })
        .withMessage(PASSWORD_IS_INVALID),
];
const getValidationErrors = (req) => {
    const validationErrors = (0, express_validator_1.validationResult)(req);
    if (!validationErrors.isEmpty()) {
        return validationErrors.mapped();
    }
    return null;
};
exports.getValidationErrors = getValidationErrors;
//# sourceMappingURL=validation.js.map