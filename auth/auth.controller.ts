import { Request, Response } from "express";
import jwt from "jsonwebtoken";

import { getValidationErrors } from "./../utils/validation";
const authService = require("./auth.service");

class AuthController {
  login = async (req: Request, res: Response) => {
    try {
      const errors = getValidationErrors(req);
      if (errors) {
        return res.status(400).json({
          errors,
        });
      }
      const { email, password } = req.body;
      const user = await authService.login(email, password);
      res.json({ user });
    } catch (error) {
      res.status(400).json({ error });
    }
  };

  register = async (req: Request, res: Response) => {
    try {
      const errors = getValidationErrors(req);
      if (errors) {
        return res.status(400).json({
          errors,
        });
      }
      const { user } = req.body;
      const createdUser = await authService.register(user);
      res.status(201).json({ user: createdUser });
    } catch (error) {
      res.status(400).json({ error });
    }
  };

  renewAuth = async (req: Request, res: Response) => {
    try {
      const { token } = req.body;
      const result = jwt.verify(token, process.env.JWT_SECRET || "");
      if (typeof result !== "string") {
        const user = await authService.renewAuth(result.email);
        return res.json({ user });
      }
    } catch (error) {
      res.status(400).json({ error: "Token is invalid" });
    }
  };
}

const authController = new AuthController();

module.exports = authController;
