import { UserEntity } from "types/User";
import jwt from "jsonwebtoken";

const User = require("./../models/user.model");

class AuthService {
  login = async (email: string, password: string) => {
    const user = await User.findOne({ email });
    if (user?.email) {
      const isPasswordValid = await user.isPasswordValid(password);
      if (isPasswordValid) {
        const token = jwt.sign({ email }, process.env.JWT_SECRET || "", {
          expiresIn: 1000000,
        });
        const userToReturn = { ...user.toJSON(), ...{ token } };
        delete userToReturn.password;
        return userToReturn;
      } else {
        throw "Credentials are invalid";
      }
    } else {
      throw "Credentials are invalid";
    }
  };

  register = async (user: UserEntity) => {
    const candidate = await User.findOne({ email: user.email });

    if (!candidate) {
      await User({ ...user, roles: ["user"] }).save();
      const createdUser = await User.findOne({ email: user.email });
      const token = jwt.sign(
        { email: createdUser.email },
        process.env.JWT_SECRET || "",
        {
          expiresIn: 1000000,
        }
      );
      const userToReturn = { ...createdUser.toJSON(), ...{ token } };
      delete userToReturn.password;
      return userToReturn;
    } else {
      throw "User already exists";
    }
  };

  renewAuth = async (email: string) => {
    const user = await User.findOne({ email });
    delete user.password;
    return user;
  };
}

const authService = new AuthService();

module.exports = authService;
