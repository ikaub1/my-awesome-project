import { Router } from "express";

import { loginValidation, registerValidation } from "./../utils/validation";

const authController = require("./auth.controller");

const router = Router();

router.post("/sign-up", registerValidation, authController.register);

router.post("/sign-in", loginValidation, authController.login);

router.post("/renew", authController.renewAuth);

module.exports = router;
