import { Button } from "components/Button/Button";
import React from "react";

import { NewsEntity } from "types/News";

type NewsProps = {
  news?: NewsEntity;
  type?: "news" | "add-news";
  onClick?: () => void;
  isShowDelete?: boolean;
  onDelete?: (id: string) => () => void;
};

export const News: React.FC<NewsProps> = ({
  news,
  type = "news",
  onClick,
  isShowDelete = false,
  onDelete,
}) => {
  return (
    <div className="news" onClick={onClick}>
      {type === "news" ? (
        <>
          <h3 className="news__title">
            {news?.title}{" "}
            {isShowDelete && onDelete && (
              <Button
                onClick={onDelete(news?._id || "")}
                className="news__btn"
                theme="delete"
              >
                &times;
              </Button>
            )}
          </h3>
          <div className="news__description">{news?.description}</div>
        </>
      ) : (
        <div className="news__add">+</div>
      )}
    </div>
  );
};
