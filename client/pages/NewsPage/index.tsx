import { NewsModal } from "components/Modal/modals/NewsModal";
import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import { selectUser } from "store/auth/auth.selectors";
import { modals } from "store/modals/modals.actions";
import { deleteNews, fetchNews } from "store/news/news.actions";
import { selectNews } from "store/news/news.selectors";
import { isAdmin } from "utils/admin.utils";
import { News } from "./components/News";

export const NewsPage: React.FC = () => {
  const user = useSelector(selectUser);
  const news = useSelector(selectNews);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!news.length) dispatch(fetchNews.started());
  }, []);

  const handleAddNews = () => {
    dispatch(modals.show(NewsModal));
  };

  const handleDeleteNews = (id: string) => () => {
    dispatch(deleteNews.started({ id }));
  };

  return (
    <div className="news-page">
      {news.map((item) => (
        <News
          onDelete={handleDeleteNews}
          isShowDelete={isAdmin(user)}
          news={item}
          key={item.title}
        />
      ))}
      {isAdmin(user) && <News onClick={handleAddNews} type="add-news" />}
    </div>
  );
};
