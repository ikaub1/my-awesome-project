export * from "./AuthPage/AuthPage";
export * from "./UsersPage/UsersPage";
export * from "./NewsPage";
export * from "./CoursesPage";
export * from "./CoursePage";
