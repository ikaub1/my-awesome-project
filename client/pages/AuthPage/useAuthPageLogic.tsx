import { useDispatch, useSelector } from "react-redux";
import { useFormik } from "formik";
import * as Yup from "yup";

import { signIn } from "store/auth/auth.actions";
import { selectUser } from "store/auth/auth.selectors";

type FormState = {
  email: string;
  password: string;
};

export const useAuthPageLogic = () => {
  const dispatch = useDispatch();
  const user = useSelector(selectUser);

  const handleSubmit = (values: FormState) => {
    dispatch(signIn.started(values));
  };

  const validationSchema = Yup.object({
    email: Yup.string()
      .required("Email cannot be empty")
      .email("Email is incorrect, try again please"),
    password: Yup.string()
      .required("Password cannot be empty")
      .min(8, "Password must be 8 characters in length minimum"),
  });

  const formik = useFormik<FormState>({
    initialValues: {
      email: "",
      password: "",
    },
    onSubmit: handleSubmit,
    validationSchema,
    validateOnChange: false,
    validateOnBlur: false,
  });

  return { user, ...formik };
};
