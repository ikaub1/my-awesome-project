import React from "react";
import { Navigate } from "react-router";

import { useAuthPageLogic } from "./useAuthPageLogic";

import { Input } from "components/Input";
import { Button } from "components/Button/Button";
import { isAdmin } from "utils/admin.utils";

export const AuthPage: React.FC = () => {
  const { user, handleSubmit, values, handleChange, errors } =
    useAuthPageLogic();

  if (user) return <Navigate to={isAdmin(user) ? "/users" : "/news"} />;

  return (
    <div className="auth">
      <h1 className="auth__title">Welcome to the CoursePresenter!</h1>
      <h2 className="auth__subtitle">
        Please, log in using the form below to start your journey
      </h2>
      <form onSubmit={handleSubmit} className="auth__form">
        <Input
          value={values.email}
          onChange={handleChange}
          className="auth__input"
          label="Email"
          type="email"
          name="email"
          error={errors.email}
        />
        <Input
          value={values.password}
          onChange={handleChange}
          className="auth__input"
          label="Password"
          type="password"
          name="password"
          error={errors.password}
        />
        <Button type="submit" className="auth__btn">
          Sign in
        </Button>
      </form>
    </div>
  );
};
