import { CourseEntity } from "types/Course";

export const courses: CourseEntity[] = [
  {
    image:
      "https://media.istockphoto.com/photos/programming-code-abstract-technology-background-of-software-developer-picture-id1224500457?k=20&m=1224500457&s=170667a&w=0&h=XzYyHReOY2K1R-rZeQIUXGHLsOJuFHLYtRL7AWR-6GY=",
    title: "Learn jQuery in 30 days",
    shortDescription:
      "jQuery is extremely powerful library for frontend development",
    description:
      "From this course you will get everything you need to develop your skills in the frontend development field. jQuery became very popular at the moment it was released and with this course you will have an opportunity to find out why. Join our community and become a professional developer for as soon as 30 days!",
    video: "vfBmYfnjLho",
    id: "1",
  },
  {
    image:
      "https://media.istockphoto.com/vectors/programming-code-application-window-vector-id1124838925?k=20&m=1124838925&s=612x612&w=0&h=lKkgavYLrbDtSnQivxMm3_4ThyovkQEFvbOb2Dv3O6g=",
    title: "Master DevOps with Docker, K8S, Terraform and AWS",
    shortDescription:
      "Become a professional DevOps engineer and pass certifications on the AWS website",
    description:
      "From this course you will learn how to configure IaaS using Terraform, how to manage microservices with Docker & K8S, deploy those to the cloud (PLEASE NOTE! It might be expensive for you to do that), how to effectively use the largest cloud provider on Earth called AWS.",
    video: "z8VIhIIq-kk",
    id: "2",
  },
  {
    image: "https://i.stack.imgur.com/hx1uP.png",
    title: "Automation testing for Java APIs",
    shortDescription:
      "Learn how to test Java APIs using the very popular Selenium driver",
    description:
      "Testing is extremely important when developing new features. This course will teach you how to implement a TDD approach correctly, structure your tests, deliver only tested and qualitative features and much more. Join us and explore the Java Testing world!",
    video: "EWUg0kJcrBA",
    id: "3",
  },
];
