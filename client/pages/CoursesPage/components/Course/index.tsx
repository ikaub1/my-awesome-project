import React from "react";
import { useNavigate } from "react-router-dom";

import { CourseEntity } from "types/Course";
import { Button } from "components/Button/Button";

interface CourseProps {
  course: CourseEntity;
}

export const Course: React.FC<CourseProps> = ({ course }) => {
  const navigate = useNavigate();

  return (
    <div className="course">
      <img src={course.image} width="200" height="200" />
      <div className="course__desc">
        <h3 className="course__title">{course.title}</h3>
        <div className="course__shortdesc">{course.shortDescription}</div>
        <Button onClick={() => navigate(course.id)} className="course__open">
          Learn more
        </Button>
      </div>
    </div>
  );
};
