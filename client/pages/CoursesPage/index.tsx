import React from "react";
import { Course } from "./components/Course";

import { courses } from "./courses";

export const CoursesPage: React.FC = () => {
  return (
    <div className="courses">
      {courses.map((course) => (
        <Course key={course.id} course={course} />
      ))}
    </div>
  );
};
