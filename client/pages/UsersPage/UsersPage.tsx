import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { selectUsers } from "store/users/users.selectors";
import { deleteUser, fetchUsers, sendEmail } from "store/users/users.actions";
import { UserCard } from "./components/UserCard";

export const UsersPage: React.FC = () => {
  const dispatch = useDispatch();
  const users = useSelector(selectUsers);

  useEffect(() => {
    if (!users.length) dispatch(fetchUsers.started());
  }, []);

  const handleDeleteUser = (userId: string) => () => {
    dispatch(deleteUser.started({ userId }));
  };

  const handleSendEmail = (id: string) => () => {
    dispatch(sendEmail.started({ userId: id }));
  };

  return (
    <div className="users">
      {users.map((user) => (
        <UserCard
          onEmailSend={handleSendEmail}
          onDelete={handleDeleteUser}
          key={user._id}
          user={user}
        />
      ))}
    </div>
  );
};
