import React from "react";
import { User } from "types/User";
import { Button } from "components/Button/Button";

interface UserCardProps {
  user: User;
  onDelete: (id: string) => () => void;
  onEmailSend: (id: string) => () => void;
}

const displayConfig = {
  email: "Email",
  firstName: "First Name",
  middleName: "Middle Name",
  lastName: "Last Name",
};

const getUserRoles = (roles: string[]) => {
  return roles.length > 1 ? roles.join(", ") : roles[0];
};

export const UserCard: React.FC<UserCardProps> = ({
  user,
  onDelete,
  onEmailSend,
}: UserCardProps) => {
  return (
    <div className="user-card">
      <h2 className="user-card__roles">Roles: {getUserRoles(user.roles)}</h2>
      <table className="user-card__table">
        <tbody>
          {Object.entries(displayConfig).map(([value, label]) => (
            <tr className="user-card__table-row" key={value}>
              <th className="user-card__table-title">{label}</th>
              <td className="user-card__table-data">{user[value]}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <div className="user-card__btn-container">
        <Button onClick={onDelete(user._id)} theme="delete">
          Delete user
        </Button>
        <Button className="user-card__send" onClick={onEmailSend(user._id)}>
          Send email
        </Button>
      </div>
    </div>
  );
};
