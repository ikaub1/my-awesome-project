import React from "react";
import { useParams } from "react-router-dom";

import { courses } from "pages/CoursesPage/courses";
import { VideoPlayer } from "components/VideoPlayer";

export const CoursePage: React.FC = () => {
  const params = useParams();

  const course = courses.find((c) => c.id === params.id);

  console.log(course);

  return (
    <div className="course-page">
      <h1 className="course-page__title">{course?.title}</h1>
      <div className="course-page__description">{course?.description}</div>
      <VideoPlayer videoId={course?.video || ""} />
    </div>
  );
};
