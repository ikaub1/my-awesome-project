import React, { useEffect } from "react";
import {
  Routes,
  Route,
  Navigate,
  RouteProps,
  Outlet,
  useLocation,
} from "react-router-dom";
import { Provider, useDispatch, useSelector } from "react-redux";

import "./App.scss";
import {
  AuthPage,
  UsersPage,
  NewsPage,
  CoursesPage,
  CoursePage,
} from "./pages";
import { store } from "./store";
import { selectUser } from "./store/auth/auth.selectors";
import { Modal } from "./components/Modal";
import { renewAuth } from "store/auth/auth.actions";
import { Header } from "./components/Header";

const WithHeader: React.FC = () => {
  return (
    <>
      <Header />
      <Outlet />
    </>
  );
};

const ProtectedRoute: React.FC<RouteProps> = ({ element }: RouteProps) => {
  const dispatch = useDispatch();
  const user = useSelector(selectUser);
  const { pathname } = useLocation();

  useEffect(() => {
    if (!user) dispatch(renewAuth.started());
  }, []);

  if (!user) return null;

  return user && element ? element : <Navigate to="/" />;
};

export const App: React.FC = () => {
  return (
    <Provider store={store}>
      <Modal />
      <Routes>
        <Route path="/" element={<AuthPage />} />
        <Route element={<WithHeader />}>
          <Route
            path="/users"
            element={<ProtectedRoute element={<UsersPage />} />}
          />
          <Route
            path="/news"
            element={<ProtectedRoute element={<NewsPage />} />}
          />
          <Route path="/courses">
            <Route
              path=""
              element={<ProtectedRoute element={<CoursesPage />} />}
            />
            <Route
              path=":id"
              element={<ProtectedRoute element={<CoursePage />} />}
            />
          </Route>
        </Route>
        <Route path="*" element={<Navigate to="/" />} />
      </Routes>
    </Provider>
  );
};
