import { all, fork } from "@redux-saga/core/effects";
import { SagaIterator } from "@redux-saga/types";
import { createStore, combineReducers, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";

import { authReducer } from "./auth/auth.reducer";
import { authWatchers } from "./auth/auth.watchers";
import { modalsReducer } from "./modals/modals.reducer";
import { modalsWatchers } from "./modals/modals.watchers";
import { newsReducer } from "./news/news.reducer";
import { newsWatchers } from "./news/news.watchers";
import { usersReducer } from "./users/users.reducer";
import { usersWatchers } from "./users/users.watchers";

function* rootSaga(): SagaIterator {
  yield all([
    fork(authWatchers),
    fork(usersWatchers),
    fork(modalsWatchers),
    fork(newsWatchers),
  ]);
}

const rootReducer = combineReducers({
  auth: authReducer,
  users: usersReducer,
  modals: modalsReducer,
  news: newsReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);
