import { SagaIterator } from "@redux-saga/types";
import { call } from "@redux-saga/core/effects";

import { API, createRequestHandler, requestCreator } from "api";
import { addNews, fetchNews, deleteNews } from "./news.actions";

const fetchNewsRequest = createRequestHandler(requestCreator.get, fetchNews);
const addNewsRequest = createRequestHandler(requestCreator.post, addNews);
const deleteNewsRequest = createRequestHandler(
  requestCreator.delete,
  deleteNews
);

export function* fetchNewsWorker(): SagaIterator {
  yield call(fetchNewsRequest, API.news());
}

export function* addNewsWorker(
  action: ReturnType<typeof addNews.started>
): SagaIterator {
  const { title, description } = action.payload;
  yield call(addNewsRequest, API.news(), { title, description });
}

export function* deleteNewsWorker(
  action: ReturnType<typeof deleteNews.started>
): SagaIterator {
  const { id } = action.payload;
  yield call(deleteNewsRequest, API.newsById(id));
}
