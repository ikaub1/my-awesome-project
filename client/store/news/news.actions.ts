import { createActionAsync } from "store/utils";
import { NewsEntity } from "types/News";

type FetchNewsStarted = void;
type FetchNewsDone = NewsEntity[];
export const fetchNews = createActionAsync<FetchNewsStarted, FetchNewsDone>(
  "news => FETCH_NEWS"
);

type AddNewsStarted = { title: string; description: string };
type AddNewsDone = { news: NewsEntity };
export const addNews = createActionAsync<AddNewsStarted, AddNewsDone>(
  "news => ADD_NEWS"
);

type DeleteNewsStarted = { id: string };
type DeleteNewsDone = void;
export const deleteNews = createActionAsync<DeleteNewsStarted, DeleteNewsDone>(
  "news => DELETE_NEWS"
);
