import { all, fork, takeLatest } from "@redux-saga/core/effects";
import { SagaIterator } from "@redux-saga/types";

import { addNews, deleteNews, fetchNews } from "./news.actions";
import {
  fetchNewsWorker,
  addNewsWorker,
  deleteNewsWorker,
} from "./news.workers";

export function* fetchNewsWatcher(): SagaIterator {
  yield takeLatest(fetchNews.started, fetchNewsWorker);
}

export function* addNewsWatcher(): SagaIterator {
  yield takeLatest(addNews.started, addNewsWorker);
}

export function* deleteNewsWatcher(): SagaIterator {
  yield takeLatest(deleteNews.started, deleteNewsWorker);
}

export function* newsWatchers(): SagaIterator {
  yield all([
    fork(fetchNewsWatcher),
    fork(addNewsWatcher),
    fork(deleteNewsWatcher),
  ]);
}
