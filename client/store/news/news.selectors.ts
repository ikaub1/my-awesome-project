import { createSelector } from "reselect";
import { RootState } from "store";

const newsSelector = (state: RootState) => state.news;

export const selectNews = createSelector(newsSelector, (state) => state.news);
