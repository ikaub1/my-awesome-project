import { NewsEntity } from "types/News";
import { reducerWithInitialState } from "typescript-fsa-reducers";
import { addNews, deleteNews, fetchNews } from "./news.actions";

export type NewsState = {
  news: NewsEntity[];
};

const initialState: NewsState = {
  news: [],
};

export const newsReducer = reducerWithInitialState(initialState)
  .case(fetchNews.done, (state, { result }) => ({ ...state, ...result }))
  .case(addNews.done, (state, { result }) => ({
    ...state,
    news: [...state.news, result.news],
  }))
  .case(deleteNews.started, (state, { id }) => ({
    ...state,
    news: state.news.filter((news) => news._id !== id),
  }));
