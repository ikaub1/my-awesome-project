import { User } from "types/User";
import { createActionAsync } from "store/utils";

type FetchUsersStarted = void;
type FetchUsersDone = User[];
export const fetchUsers = createActionAsync<FetchUsersStarted, FetchUsersDone>(
  "users => FETCH_USERS"
);

type DeleteUserStarted = { userId: string };
type DeleteUserDone = void;
export const deleteUser = createActionAsync<DeleteUserStarted, DeleteUserDone>(
  "users => DELETE_USER"
);

type SendEmailStarted = { message: string } | { userId: string };
type SendEmailDone = void;
export const sendEmail = createActionAsync<SendEmailStarted, SendEmailDone>(
  "users => SEND_EMAIL"
);
