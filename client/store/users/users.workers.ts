import { call, select, put } from "@redux-saga/core/effects";
import { SagaIterator } from "@redux-saga/types";

import { API, createRequestHandler, requestCreator } from "api";
import { fetchUsers, deleteUser, sendEmail } from "./users.actions";
import { selectOperatedUser } from "store/users/users.selectors";
import { modals } from "store/modals/modals.actions";
import { DeleteModal } from "components/Modal/modals/DeleteModal";
import { selectIsModalShown } from "store/modals/modals.selectors";
import { selectUser } from "store/auth/auth.selectors";
import { SendEmailModal } from "components/Modal/modals/SendEmailModal";

const fetchUsersRequest = createRequestHandler(requestCreator.get, fetchUsers);
const deleteUserRequest = createRequestHandler(
  requestCreator.delete,
  deleteUser
);
const sendEmailRequest = createRequestHandler(requestCreator.post, sendEmail);

export function* fetchUsersWorker(): SagaIterator {
  yield call(fetchUsersRequest, API.users());
}

export function* deleteUserWorker(): SagaIterator {
  const user = yield select(selectOperatedUser);
  const isModalShown = yield select(selectIsModalShown);
  if (!isModalShown) yield put(modals.show(DeleteModal));
  else yield call(deleteUserRequest, API.userById(user._id));
}

export function* sendEmailWorker(
  action: ReturnType<typeof sendEmail.started>
): SagaIterator {
  const currentUser = yield select(selectUser);
  const operatedUser = yield select(selectOperatedUser);
  const isModalShown = yield select(selectIsModalShown);

  const data = action.payload;
  if (!isModalShown) yield put(modals.show(SendEmailModal));
  else
    yield call(sendEmailRequest, API.sendEmail(), {
      from: currentUser.email,
      to: operatedUser.email,
      message: "message" in data && data.message,
    });
}
