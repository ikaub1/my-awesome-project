import { all, fork, takeLatest } from "@redux-saga/core/effects";
import { SagaIterator } from "@redux-saga/types";
import { fetchUsers, deleteUser, sendEmail } from "./users.actions";
import {
  fetchUsersWorker,
  deleteUserWorker,
  sendEmailWorker,
} from "./users.workers";

export function* fetchUsersWatcher(): SagaIterator {
  yield takeLatest(fetchUsers.started, fetchUsersWorker);
}

export function* deleteUserWatcher(): SagaIterator {
  yield takeLatest(deleteUser.started, deleteUserWorker);
}

export function* sendEmailWatcher(): SagaIterator {
  yield takeLatest(sendEmail.started, sendEmailWorker);
}

export function* usersWatchers(): SagaIterator {
  yield all([
    fork(fetchUsersWatcher),
    fork(deleteUserWatcher),
    fork(sendEmailWatcher),
  ]);
}
