import { User } from "types/User";
import { reducerWithInitialState } from "typescript-fsa-reducers";
import { fetchUsers, deleteUser, sendEmail } from "./users.actions";

export type UsersState = {
  users: User[];
  userToOperate: User | null;
};

const initialState: UsersState = {
  users: [],
  userToOperate: null,
};

export const usersReducer = reducerWithInitialState(initialState)
  .case(fetchUsers.done, (state, { result }) => ({ ...state, users: result }))
  .case(deleteUser.started, (state, { userId }) => ({
    ...state,
    userToOperate: state.users.find((user) => user._id === userId) || null,
  }))
  .case(deleteUser.done, (state) => ({
    ...state,
    userToOperate: null,
    users: state.users.filter((user) => user._id !== state.userToOperate?._id),
  }))
  .case(sendEmail.started, (state, payload) => ({
    ...state,
    userToOperate:
      "userId" in payload
        ? state.users.find((user) => user._id === payload.userId) || null
        : state.userToOperate,
  }))
  .case(sendEmail.done, (state) => ({ ...state, userToOperate: null }));
