import { createSelector } from "reselect";

import { RootState } from "store";

const usersSelector = (state: RootState) => state.users;

export const selectUsers = createSelector(
  usersSelector,
  (state) => state.users
);

export const selectOperatedUser = createSelector(
  usersSelector,
  (state) => state.userToOperate
);
