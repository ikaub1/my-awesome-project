import actionCreatorFactory from "typescript-fsa";

export const createAction = actionCreatorFactory();

export const createActionAsync = createAction.async;
