import { SagaIterator } from "@redux-saga/types";
import { call } from "@redux-saga/core/effects";

import { createRequestHandler, API, requestCreator } from "api";

import { signIn, renewAuth } from "./auth.actions";

const loginRequest = createRequestHandler(requestCreator.post, signIn);
const renewAuthRequest = createRequestHandler(requestCreator.post, renewAuth);

export function* signInWorker(
  action: ReturnType<typeof signIn.started>
): SagaIterator {
  const { email, password } = action.payload;
  yield call(loginRequest, API.login(), { email, password });
}

export function* renewAuthWorker(): SagaIterator {
  const token = localStorage.getItem("token");
  if (token) {
    yield call(renewAuthRequest, API.renewAuth(), { token });
  }
}
