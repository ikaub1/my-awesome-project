import { createActionAsync } from "./../utils";
import { User } from "./../../types/User";

type SignInStarted = { email: string; password: string };
type SignInDone = { user: User };
export const signIn = createActionAsync<SignInStarted, SignInDone>(
  "auth => SIGN_IN"
);

type RenewAuthStarted = void;
type RenewAuthDone = User | null;
export const renewAuth = createActionAsync<RenewAuthStarted, RenewAuthDone>(
  "auth => RENEW_AUTH"
);
