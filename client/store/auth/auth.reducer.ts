import { reducerWithInitialState } from "typescript-fsa-reducers";
import { renewAuth, signIn } from "./auth.actions";
import { User } from "./../../types/User";

export type AuthState = {
  user: User | null;
};

const initialState: AuthState = {
  user: null,
};

export const authReducer = reducerWithInitialState(initialState)
  .case(signIn.done, (state, { result }) => {
    localStorage.setItem("token", result.user.token);
    return { ...state, ...result };
  })
  .case(renewAuth.done, (state, { result }) => ({ ...state, ...result }));
