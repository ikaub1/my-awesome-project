import { all, fork, takeLatest } from "@redux-saga/core/effects";
import { SagaIterator } from "@redux-saga/types";

import { signIn, renewAuth } from "./auth.actions";
import { signInWorker, renewAuthWorker } from "./auth.workers";

function* signInWatcher(): SagaIterator {
  yield takeLatest(signIn.started, signInWorker);
}

function* renewAuthWatcher(): SagaIterator {
  yield takeLatest(renewAuth.started, renewAuthWorker);
}

export function* authWatchers(): SagaIterator {
  yield all([fork(signInWatcher), fork(renewAuthWatcher)]);
}
