import { createSelector } from "reselect";
import { RootState } from "./../../store";

const authSelector = (state: RootState) => state.auth;

export const selectUser = createSelector(authSelector, (state) => state.user);
