import { takeLatest } from "@redux-saga/core/effects";

import { SagaIterator } from "@redux-saga/types";
import { modals } from "./modals.actions";
import { modalsWorker } from "./modals.workers";

export function* modalsWatchers(): SagaIterator {
  yield takeLatest(modals.show, modalsWorker);
}
