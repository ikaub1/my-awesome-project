import { createSelector } from "reselect";
import { RootState } from "store";

const modalsSelector = (state: RootState) => state.modals;

export const selectIsModalShown = createSelector(
  modalsSelector,
  (state) => state.isShown
);
