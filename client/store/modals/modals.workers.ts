import { SagaIterator } from "@redux-saga/types";
import { modalsConfig } from "components/Modal";
import { modals } from "./modals.actions";

export function* modalsWorker(
  action: ReturnType<typeof modals.show>
): SagaIterator {
  const render = action.payload;
  modalsConfig.setRender(render);
}
