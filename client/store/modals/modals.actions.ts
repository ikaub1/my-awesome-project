import React from "react";
import { createAction } from "store/utils";

export const modals = {
  show: createAction<React.FC<any>>("modals => SHOW_MODAL"),
  hide: createAction<void>("modals => HIDE_MODAL"),
};
