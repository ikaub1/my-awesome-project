import { reducerWithInitialState } from "typescript-fsa-reducers";

import { modals } from "./modals.actions";

export type ModalsState = {
  isShown: boolean;
};

const initialState: ModalsState = {
  isShown: false,
};

export const modalsReducer = reducerWithInitialState(initialState)
  .case(modals.show, (state) => {
    return { ...state, isShown: true };
  })
  .case(modals.hide, (state) => ({ ...state, isShown: false }));
