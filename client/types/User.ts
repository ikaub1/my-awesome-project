export interface User extends Record<string, string | string[]> {
  _id: string;
  email: string;
  firstName: string;
  middleName: string;
  lastName: string;
  password: string;
  token: string;
  roles: ["user", "admin"];
}
