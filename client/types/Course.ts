export interface CourseEntity {
  title: string;
  shortDescription: string;
  description: string;
  image: string;
  video: string;
  id: string;
}
