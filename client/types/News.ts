export interface NewsEntity {
  title: string;
  description: string;
  _id: string;
}
