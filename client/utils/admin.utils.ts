import { User } from "types/User";

export const isAdmin = (user: User | null) => user?.roles.includes("admin");
