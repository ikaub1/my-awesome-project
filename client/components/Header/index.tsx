import React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

import { Button } from "components/Button/Button";
import { selectUser } from "store/auth/auth.selectors";
import { isAdmin } from "utils/admin.utils";

export const Header: React.FC = () => {
  const user = useSelector(selectUser);

  return (
    <header className="header">
      <nav>
        <Link className="header__link" to="/news">
          News
        </Link>
        <Link to="/courses" className="header__link">
          Courses
        </Link>
        {isAdmin(user) && (
          <Link className="header__link" to="/users">
            Users
          </Link>
        )}
      </nav>
      <Button>Log out</Button>
    </header>
  );
};
