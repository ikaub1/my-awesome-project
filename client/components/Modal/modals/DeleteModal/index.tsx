import React from "react";
import { useSelector } from "react-redux";

import { Button } from "components/Button/Button";
import { selectOperatedUser } from "store/users/users.selectors";
import { ModalProps } from "components/Modal";
import { deleteUser } from "store/users/users.actions";

export const DeleteModal: React.FC<ModalProps> = ({ dispatch, onClose }) => {
  const user = useSelector(selectOperatedUser);

  const handleClose = () => {
    dispatch && dispatch(deleteUser.started({ userId: user?._id || "" }));
    onClose();
  };

  return (
    <div className="modals__delete">
      Are you sure you want to delete{" "}
      {`${user?.firstName} ${user?.middleName} ${user?.lastName}`}?
      <Button
        onClick={handleClose}
        className="modals__delete-btn"
        theme="delete"
      >
        Delete permanently
      </Button>
    </div>
  );
};
