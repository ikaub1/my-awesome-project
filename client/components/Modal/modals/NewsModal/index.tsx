import React, { useState } from "react";
import { useDispatch } from "react-redux";

import { Input } from "components/Input";
import { Button } from "components/Button/Button";
import { addNews } from "store/news/news.actions";
import { ModalProps } from "components/Modal";

export const NewsModal: React.FC<ModalProps> = ({ onClose }) => {
  const dispatch = useDispatch();

  const [news, setNews] = useState({
    title: "",
    description: "",
  });

  const handleChange = (e: React.ChangeEvent<any>) => {
    setNews({ ...news, [e.target.name]: e.target.value });
  };

  const handleAddNews = () => {
    dispatch(addNews.started({ ...news }));
    onClose();
  };

  return (
    <div className="news-modal">
      <h3>Add News</h3>
      <Input
        onChange={handleChange}
        name="title"
        value={news.title}
        label="Title"
      />
      <textarea
        name="description"
        value={news.description}
        onChange={handleChange}
        className="send-email__message"
        cols={35}
        rows={8}
      ></textarea>
      <Button onClick={handleAddNews}>Add</Button>
    </div>
  );
};
