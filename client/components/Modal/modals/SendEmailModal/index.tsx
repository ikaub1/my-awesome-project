import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import { sendEmail } from "store/users/users.actions";
import { selectOperatedUser } from "store/users/users.selectors";
import { Button } from "components/Button/Button";
import { ModalProps } from "components/Modal";

export const SendEmailModal: React.FC<ModalProps> = ({ onClose }) => {
  const dispatch = useDispatch();
  const operatedUser = useSelector(selectOperatedUser);
  const [message, setMessage] = useState("");

  const handleClick = () => {
    dispatch(sendEmail.started({ message }));
    onClose();
  };

  const handleChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setMessage(e.target.value);
  };

  return (
    <div className="send-email">
      <h3 className="send-email__title">Email to {operatedUser?.email}</h3>
      <p>Text:</p>
      <textarea
        value={message}
        onChange={handleChange}
        className="send-email__message"
        cols={35}
        rows={8}
      ></textarea>
      <Button onClick={handleClick} className="send-email__btn">
        Send
      </Button>
    </div>
  );
};
