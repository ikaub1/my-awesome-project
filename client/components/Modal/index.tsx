import React, { Dispatch } from "react";
import { useSelector, useDispatch } from "react-redux";
import { modals } from "store/modals/modals.actions";

import { selectIsModalShown } from "store/modals/modals.selectors";
import { AsyncActionCreators } from "typescript-fsa";

export type ModalProps = {
  onClose: () => void;
  dispatch?: Dispatch<any>;
};

class ModalsConfig {
  Render: React.FC<ModalProps> | null = null;

  setRender = (render: React.FC<ModalProps>) => {
    this.Render = render;
  };
}

export const modalsConfig = new ModalsConfig();

export const Modal: React.FC = () => {
  const dispatch = useDispatch();
  const isShown = useSelector(selectIsModalShown);

  if (!isShown) return null;

  const handleClose = () => {
    dispatch(modals.hide());
  };

  return (
    <div className="modal">
      <div className="modal__overlay" onClick={handleClose} />
      <div className="modal__content">
        {modalsConfig.Render && (
          <modalsConfig.Render onClose={handleClose} dispatch={dispatch} />
        )}
      </div>
    </div>
  );
};
