import React from "react";
import cn from "classnames";

interface ButtonProps extends React.HTMLProps<HTMLButtonElement> {
  type?: "button" | "submit" | "reset";
  className?: string;
  theme?: "default" | "delete";
}

export const Button: React.FC<ButtonProps> = ({
  children,
  type,
  className = "",
  theme = "default",
  ...rest
}: ButtonProps) => {
  return (
    <button
      className={cn("btn", `btn__${theme}`, className)}
      type={type}
      {...rest}
    >
      {children}
    </button>
  );
};
