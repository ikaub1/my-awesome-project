import React from "react";
import cn from "classnames";

interface InputProps extends React.HTMLProps<HTMLInputElement> {
  label?: string;
  className?: string;
  error?: string;
}

export const Input: React.FC<InputProps> = ({
  label,
  value,
  className = "",
  error = "",
  ...rest
}: InputProps) => {
  return (
    <label className={cn("input", className)}>
      <input className="input__control" {...rest} />
      <span className={cn("input__label", { "input__label-value": !!value })}>
        {label}
      </span>
      {error && <span className="input__error">{error}</span>}
    </label>
  );
};
