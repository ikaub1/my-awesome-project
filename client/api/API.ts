class API {
  login = () => `/auth/sign-in`;
  renewAuth = () => `/auth/renew`;
  users = () => `/users`;
  userById = (userId: string) => `${this.users()}/${userId}`;
  news = () => `/news`;
  newsById = (id: string) => `${this.news()}/${id}`;
  sendEmail = () => `/emails/send`;
}

const api = new API();

export default api;
