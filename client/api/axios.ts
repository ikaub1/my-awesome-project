import axios, { AxiosRequestConfig, AxiosResponse } from "axios";

const instance = axios.create({
  baseURL: "http://localhost:4000/api",
  headers: {
    "Content-Type": "application/json",
  },
});

const requestHandler = (request: AxiosRequestConfig) => {
  request.headers = {
    ...request.headers,
    Authorization: `Bearer ${localStorage.getItem("token")}` || "",
  };
  return request;
};

const responseHandler = (response: AxiosResponse) => {
  if (response.status === 401) {
    window.location.href = "/";
  }
  return response;
};

const errorHandler = (error: Error) => {
  return Promise.reject(error);
};

instance.interceptors.request.use(requestHandler, errorHandler);
instance.interceptors.response.use(responseHandler, errorHandler);

export default instance;
