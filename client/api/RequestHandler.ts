import { AsyncActionCreators } from "typescript-fsa";
import { put } from "@redux-saga/core/effects";

import { SagaIterator } from "@redux-saga/types";

export const createRequestHandler = (
  callFn: CallableFunction,
  action: AsyncActionCreators<any, any>
) => {
  return function* (url: string, data?: unknown): SagaIterator {
    try {
      const result = yield callFn(url, data);
      yield put(action.done({ result }));
    } catch (error) {
      yield put(action.failed({ params: {}, error: {} }));
    }
  };
};
