import axios from "./axios";

class RequestCreator {
  get = async (url: string) => {
    const response = await axios.get(url);
    return response.data;
  };

  post = async (url: string, data: unknown) => {
    const response = await axios.post(url, data);
    return response.data;
  };

  delete = async (url: string) => {
    const response = await axios.delete(url);
    return response.data;
  };

  patch = async (url: string, data: unknown) => {
    const response = await axios.patch(url, data);
    return response.data;
  };
}

const requestCreator = new RequestCreator();

export default requestCreator;
